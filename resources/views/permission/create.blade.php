@extends('layouts.vaslayout')
@section('content')

@include('layouts.roleandpermissionheader')
@yield('headercontent')

@include('notificationmessage.failed')
@yield('failed')
@include('notificationmessage.success')
@yield('success')
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
	<div class="row">
		<div class="col-xl-12">
			<!--begin::Portlet-->
			<div class="kt-portlet kt-portlet--head--noborder kt-portlet--height-fluid">
				<div class="kt-portlet__head">
					<div class="kt-portlet__head-label">
						<h3 class="kt-portlet__head-title">
							PERMISSION REGISTRATION
						</h3>
					</div>
				</div>
				<!--begin::Form-->
				<form class="kt-form kt-form--label-right" id="kt_form_1" method="POST" action="{{route('permission.store')}}">

                        @csrf
					<div class="kt-portlet__body">
						<div class="form-group row">
							<div class="col-lg-6">
								<label class="">Permission Name:</label>
									<div class="input-group">
									<input type="text" name="name" class="form-control" placeholder="Enter permission name">
										<div class="input-group-append"><a href="#" class="btn btn-brand btn-icon"><i class="la la-key"></i></a></div>
									</div>
								<span class="form-number text-danger">{{$errors->first('name')}}</span>
							</div>
						</div>
					</div>
					<div class="kt-portlet__foot">
						<div class="kt-form__actions">
							<div class="row">
								<div class="col-lg-6">
									<button type="submit" class="btn btn-primary">Save</button>
								</div>
								<div class="col-lg-6 kt-align-right">
									<button type="reset" class="btn btn-danger">Delete</button>
								</div>
							</div>
						</div>
					</div>
				</form>
				<!--end::Form-->
			</div>
            <!--end::Portle -->
        </div>
    </div>
</div>
@endsection
