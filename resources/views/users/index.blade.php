@extends('layouts.vaslayout')

@section('content')
@include('notificationmessage.failed')
@yield('failed')
@include('notificationmessage.success')
@yield('success')
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
							
							<div class="kt-portlet kt-portlet--mobile">
								<div class="kt-portlet__head kt-portlet__head--lg">
									<div class="kt-portlet__head-label">
										<span class="kt-portlet__head-icon">
											<i class="kt-font-brand flaticon2-line-chart"></i>
										</span>
										<h3 class="kt-portlet__head-title">
											USERS
											<small>USERS WITH THEIR ROLES</small>
										</h3>
									</div>
									<div class="kt-portlet__head-toolbar">
										<div class="kt-portlet__head-wrapper">
											<div class="kt-portlet__head-actions">
												
												&nbsp;
												<a href="{{route('users.create')}}" class="btn btn-brand btn-elevate btn-icon-sm">
													<i class="la la-plus"></i>
													New User
												</a>
											</div>
										</div>
									</div>
								</div>
								<div class="kt-portlet__body">

									<!--begin: Search Form -->
									<div class="kt-form kt-form--label-right kt-margin-t-20 kt-margin-b-10">
										<div class="row align-items-center">
											<div class="col-xl-8 order-2 order-xl-1">
												<div class="row align-items-center">
													<div class="col-md-4 kt-margin-b-20-tablet-and-mobile">
														<div class="kt-input-icon kt-input-icon--left">
															<input type="text" class="form-control" placeholder="Search..." id="generalSearch">
															<span class="kt-input-icon__icon kt-input-icon__icon--left">
																<span><i class="la la-search"></i></span>
															</span>
														</div>
													</div>
													
													
												</div>
											</div>
											
										</div>
									</div>

									<!--end: Search Form -->
								</div>
								<div class="kt-portlet__body kt-portlet__body--fit">

									<!--begin: Datatable -->
									<table class="kt-datatable" id="html_table" width="100%">
										<thead>
											<tr>
												<th >USER ID</th>
												<th title="Field #2">EMAIL</th>
												<th title="Field #3">FULL NAME</th>
												<th title="Field #4">PHONE</th>
												<th title="Field #6">ROLES</th>
												<th title="Field #7">SERVICE</th>
												<th title="Field #8">ACTIONS</th>
											</tr>
										</thead>
										<tbody>

											@foreach ($users as $user)
												@php
												if($user->service_id != null){
													$service = DB::table('services')->where('id','=',$user->service_id)->get()->first();
													$name=$service->service_name;

												}
												if($user->service_id == null){
													$name="No Service Assigned";
												}
												@endphp
											
											<tr>
												<td>{{ $user->id }}</td>
												<td>{{ $user->email }}</td>
												<td>{{ $user->first_name }} {{ $user->last_name }} {{ $user->middle_name }}</td>
												<td>{{ $user->phone }}</td>
												<td align="center">
												
												@foreach($user->roles as $ro)
												<h5><span class="kt-badge kt-badge--success kt-badge--inline">{{$ro->name}}</span></h5>
												<br>
                                                 @endforeach
												
												</td>
												<td align="right">{{ $name }}</td>
												<td>
												<form action="{{route('users.destroy',$user->user_id)}}" method="post" role="form">
												<a href="{{route('users.edit',$user->user_id)}}" ><i class="flaticon2-edit"></i></a>	&nbsp;	&nbsp; &nbsp;
												<input type="hidden" name="_token" value="{{csrf_token()}}">
												<input type="hidden" name="_method" value="DELETE">
												<button type="submit" class="btn btn-light btn-elevate-hover btn-icon"><i class="flaticon2-trash"></i></button>
												</form>
												</td>
											</tr>
											@endforeach
											
										</tbody>
									</table>

									<!--end: Datatable -->
								</div>
							</div>
                        </div>
@endsection