@extends('layouts.vaslayout')
@section('content')
@include('notificationmessage.failed')
@yield('failed')
@include('notificationmessage.success')
@yield('success')
<!-- begin:: Content Head -->
<div class="kt-subheader   kt-grid__item" id="kt_subheader">
							<div class="kt-subheader__main">
								
								<a href="{{route('users.index')}}" class="btn btn-label-warning btn-bold btn-sm btn-icon-h kt-margin-l-10">
									USERS INDEX
								</a>
								<div class="kt-input-icon kt-input-icon--right kt-subheader__search kt-hidden">
									<input type="text" class="form-control" placeholder="Search order..." id="generalSearch">
									<span class="kt-input-icon__icon kt-input-icon__icon--right">
										<span><i class="flaticon2-search-1"></i></span>
									</span>
								</div>
							</div>
							<div class="kt-subheader__toolbar">
								<div class="kt-subheader__wrapper">
							
									<div class="dropdown dropdown-inline" data-toggle="kt-tooltip" title="Quick actions" data-placement="left">
										<a href="#" class="btn btn-icon" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
										
										</a>
										<div class="dropdown-menu dropdown-menu-right">
										
										</div>
									</div>
								</div>
							</div>
						</div>

						<!-- end:: Content Head -->

<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
	<div class="row">
		<div class="col-xl-12">
			<!--begin::Portlet-->
			<div class="kt-portlet kt-portlet--head--noborder kt-portlet--height-fluid">
				<div class="kt-portlet__head">
					<div class="kt-portlet__head-label">
						<h3 class="kt-portlet__head-title">
							EDIT USER PROFILE
						</h3>
					</div>
				</div>
				<!--begin::Form-->
				<form class="kt-form kt-form--label-right" id="kt_form_1" method="POST" action="{{route('users.update',$id)}}">
						<input type="hidden" name="_token" value="{{csrf_token()}}">
                        <input type="hidden" name="_method" value="PUT">

					<div class="kt-portlet__body">
						<div class="form-group row">
							<div class="col-lg-6">
								<label>First Name:</label>
								<input type="text" name="first_name" class="form-control" placeholder="Enter first name" value="{{$user->first_name}}">
								<span class="form-text text-danger">{{$errors->first('first_name')}}</span>
							</div>
							<div class="col-lg-6">
								<label>Last Name:</label>
								<input type="text" name="last_name" class="form-control" placeholder="Enter last name" value="{{$user->last_name}}">
								<span class="form-text text-danger">{{$errors->first('last_name')}}</span>
							</div>
							<div class="col-lg-6">
								<label>Middle Name:</label>
								<input type="text" name="middle_name" class="form-control" placeholder="Enter middle name" value="{{$user->middle_name}}">
								<span class="form-text text-danger">{{$errors->first('middle_name')}}</span>
							</div>
						</div>
						<div class="form-group row">
							<div class="col-lg-6">
								<label class="">Email:</label>
								<input type="email" name="email"  class="form-control" placeholder="Enter email account" value="{{$user->email}}">
								<span class="form-email text-danger">{{$errors->first('email')}}</span>
							</div>
							<div class="col-lg-6">
								<label class="">Contact Number:</label>
								<input type="number" name="phone" class="form-control" placeholder="Enter contact number" value="{{$user->phone}}">
								<span class="form-number text-danger">{{$errors->first('phone')}}</span>
							</div>
						</div>
						<div class="form-group row">
							
							<div class="col-lg-6">
								<label class="">Role:</label>
								<input hidden id="userrole" type="text" value="{{$user->name}}" name="userrole">
									<select name="role" class="form-control"  >
	
										<option disabled>{{$user->name}}</option>
                                        @foreach($roles as $role)
                                        <option value="{{$role->name}}">{{$role->name}}</option>
										@endforeach
									</select>
									<span class="form-text text-danger">{{$errors->first('role')}}</span>
							</div>
							<div class="col-lg-6"id="service">
								<label class="">Services:</label>
							
									<select name="service" class="form-control" id="service">
										<option value="">Select Services</option>
                                        @foreach($services as $service)
                                        <option value="{{$service->id}}">{{$service->service_name}}</option>
										@endforeach
									</select>
									<span class="form-text text-danger">{{$errors->first('service')}}</span>
							</div>
						</div>
						
						
					</div>
					<div class="kt-portlet__foot">
						<div class="kt-form__actions">
							<div class="row">
								<div class="col-lg-6">
									<button type="submit" class="btn btn-primary">Update</button>
								</div>
								<div class="col-lg-6 kt-align-right">
									<button type="reset" class="btn btn-danger">Delete</button>
								</div>
							</div>
						</div>
					</div>
				</form>
				<!--end::Form-->
			</div>
            <!--end::Portle -->
        </div>
    </div>
</div>
@endsection