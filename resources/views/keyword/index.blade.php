
@extends('layouts.vaslayout')
@section('content')
@include('keyword.header')
@yield('headercontent')
@include('notificationmessage.failed')
@yield('failed')
@include('notificationmessage.success')
@yield('success')

<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
	<div class="kt-portlet kt-portlet--mobile">
		<div class="kt-portlet__head kt-portlet__head--lg">
			<div class="kt-portlet__head-label">
				<span class="kt-portlet__head-icon">
					<i class="kt-font-brand flaticon2-line-chart"></i>
				</span>
				<h3 class="kt-portlet__head-title">
					KEYWORDS
					<small>LIST OF KEYWORDS</small>
				</h3>
			</div>
			<div class="kt-portlet__head-toolbar">
				<div class="kt-portlet__head-wrapper">
					<div class="kt-portlet__head-actions">
						<div class="dropdown dropdown-inline">
					
						</div>
						&nbsp;
						<a href="{{route('keyword.create')}}" class="btn btn-brand btn-elevate btn-icon-sm">
							<i class="la la-plus"></i>
							New Keyword
						</a>
					</div>
				</div>
			</div>
		</div>
		<div class="kt-portlet__body">
			<!--begin: Search Form -->
		</div>
		<div class="kt-portlet__body kt-portlet__body--fit">
			<!--begin: Datatable -->
			<table class="kt-datatable" id="html_table" width="100%">
				<thead>
					<tr>
						<th title="Field #3">KEYWORD ID</th>
						<th title="Field #4">KEYWORD</th>
                        <th title="Field #7">SERVICE NAME</th>
<<<<<<< HEAD
						<th title="Field #8">ACTION</th>
=======
						<th title="Field #8">DETAIL</th>
>>>>>>> d68a435fd33baac5003520e8e90b3e878bcc328d
					</tr>
				</thead>
				<tbody>
				@foreach($keywords as $keyword)
					<tr>
						<td>{{$keyword->id}}</td>
                        <td>{{$keyword->keyword}}</td>
                        <td>{{$keyword->service_id}}</td>
                        <td>
<<<<<<< HEAD
						<form action="{{route('keyword.destroy',$keyword->id)}}" method="POST">
                		      @csrf
                		    <input type="hidden" name="_token" value="{{csrf_token()}}">
                		    <input type="hidden" name="_method" value="DELETE">
                		    <button type="submit" class="btn btn-outline-danger btn-icon mg-r-5"><div><i class="flaticon2-trash"></i></div></button>
                		</form> 
=======
							<a href="{{route('keyword.edit',$keyword->id)}}" ><i class="flaticon2-edit"></i></a>	&nbsp;	&nbsp;
							<a href="{{route('keyword.destroy',$keyword->id)}}" ><i class="flaticon2-trash"></i></a>	&nbsp;	&nbsp;
							
>>>>>>> d68a435fd33baac5003520e8e90b3e878bcc328d
						</td>
                    </tr>
                @endforeach
					
				</tbody>
			</table>
			<!--end: Datatable -->
		</div>
	</div>
</div>
@endsection