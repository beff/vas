
@extends('layouts.vaslayout')

@section('content')
@include('service.header')
@yield('headercontent')
@include('notificationmessage.failed')
@yield('failed')
@include('notificationmessage.success')
@yield('success')
@section('headerbuttons')
@parent
	<li class="kt-menu__item  kt-menu__item--active " aria-haspopup="true"><a href="{{route('service.index')}}" class="kt-menu__link "><span class="kt-menu__link-text">Service</span></a></li>
									
				
@endsection
<<<<<<< HEAD
<!-- ////////////// -->
<div class="kt-portlet kt-portlet--mobile">
	<div class="kt-portlet__head">
		<div class="kt-portlet__head-label">
		<h3 class="kt-portlet__head-title">
					GROUPS
					<small>LIST OF GROUPS NOT ASSIGNED TO {{$service->service_name}}.</small>
				</h3>
		</div>
	</div>

	<div class="kt-portlet__body">
		<!--begin: Datatable -->
		<form method="POST" action="{{route('assignedgroup',$service->id)}}">
		@csrf
		<table class="table table-striped- table-bordered table-hover" >
				<thead>
					<tr>
					<th>
						<label >
						<span id="lable">
                        <input type="checkbox" onclick="selectall()" id="checkall" >
                        </span>
                        </label>
                        </label>
					</th>
					<th>GROUP ID</th>
					<th>GROUP NAME</th>
					</tr>
				</thead>
			<tbody>
			@foreach($groups as $group)
					<tr>
						<td>
						<label class="kt-checkbox kt-checkbox--brand">
							<input name="groupid[]" value="{{$group->id}}"  type="checkbox" id="check" class="kt-group-checkable"> 
							<span></span>
                        </label>
						</td>
						<td>{{$group->id}}</td>
						<td>{{$group->group_name}}</td>
					</tr>
            @endforeach
			</tbody>
				
		</table><div class="kt-portlet__foot">
					<div class="kt-form__actions">
						<div class="row">
							<div class="col-lg-6">
								<button type="submit" class="btn btn-primary">ASSIGN CUSTOMERS TO {{$group->group_name}}.</button>
							</div>
							<div class="col-lg-6 kt-align-right">
								<button type="reset" class="btn btn-danger">Delete</button>
							</div>
						</div>
					</div>
				</div>
		</form>
		<!--end: Datatable -->
	</div>
</div>
<!-- /////////// -->

=======

<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
	<div class="kt-portlet kt-portlet--mobile">
		<div class="kt-portlet__head kt-portlet__head--lg">
			<div class="kt-portlet__head-label">
				<span class="kt-portlet__head-icon">
					<i class="kt-font-brand flaticon2-line-chart"></i>
				</span>
				<h3 class="kt-portlet__head-title">
					GROUPS
					<small>LIST OF GROUPS NOT ASSIGNED TO {{$service->service_name}}.</small>
				</h3>
			</div>
			<div class="kt-portlet__head-toolbar">
				<div class="kt-portlet__head-wrapper">
					<div class="kt-portlet__head-actions">
						
						&nbsp;
						
					</div>
				</div>
			</div>
		</div>
		<div class="kt-portlet__body">
			<!--begin: Search Form -->
			<div class="kt-form kt-form--label-right kt-margin-t-20 kt-margin-b-10">
				<div class="row align-items-center">
					<div class="col-xl-8 order-2 order-xl-1">
						<div class="row align-items-center">
							<div class="col-md-4 kt-margin-b-20-tablet-and-mobile">
								<div class="kt-input-icon kt-input-icon--left">
									<input type="text" class="form-control" placeholder="Search..." id="generalSearch">
									<span class="kt-input-icon__icon kt-input-icon__icon--left">
										<span><i class="la la-search"></i></span>
									</span>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
				<!--end: Search Form -->
			
		</div>
		<form method="POST" action="{{route('assignedgroup',$service->id)}}">
		@csrf
		<div class="kt-portlet__body kt-portlet__body--fit">
		
		
			<!--begin: Datatable -->
			<table class="kt-datatable" id="html_table" width="100%">
				<thead>
					<tr>
						<th title="Field #1">GROUP ID</th>
						<th title="Field #2">GROUP NAME</th>
						<th title="Field #3">
						<label class="kt-checkbox kt-checkbox--single kt-checkbox--solid kt-checkbox--brand">
                        <input type="checkbox"  name="select-all" id="select-all" value="" class="kt-group-checkable" >
                        <span></span>
                        </label>
						</th>
						<th title="Field #3">SELECT</th>
					</tr>
				</thead>
				<tbody>
					@foreach($groups as $group)
						<tr>
							<td>{{$group->id}}</td>
                	        <td>{{$group->group_name}}</td>
                	        <td>
							<label class="kt-checkbox kt-checkbox--brand">
								<input name="groupid[]"value="{{$group->id}}"type="checkbox"> 
								<span></span>
                	        </label>
							</td>
                	    </tr>
                	@endforeach
				</tbody>
			</table>
			<!--end: Datatable -->
		
		</div>

		</form>
	</div>
</div>
>>>>>>> d68a435fd33baac5003520e8e90b3e878bcc328d
@endsection