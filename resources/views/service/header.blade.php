@section('headercontent')
<!-- begin:: Content Head -->
<div class="kt-subheader   kt-grid__item" id="kt_subheader">
	<div class="kt-subheader__main">
	
		<h3 class="kt-subheader__title">
		{{$service->service_name}} </h3>
	
		<div class="kt-input-icon kt-input-icon--right kt-subheader__search kt-hidden">
			<input type="text" class="form-control" placeholder="Search order..." id="generalSearch">
			<span class="kt-input-icon__icon kt-input-icon__icon--right">
				<span><i class="flaticon2-search-1"></i></span>
			</span>
		</div>
	</div>
	<div class="kt-subheader__toolbar">
		<div class="kt-subheader__wrapper">
			<a href="{{route('assigngroupindex',$id)}}" class="btn kt-subheader__btn-daterange"  data-toggle="kt-tooltip" title="Assign groups to service" data-placement="left">
				<span class="kt-subheader__btn-daterange-date" >ASSIGN</span>
				<i class="flaticon2-layers-1"></i>
            </a>
            <a href="{{route('assigngroups',$id)}}" class="btn btn-label-warning btn-bold btn-sm btn-icon-h kt-margin-l-10"  data-toggle="kt-tooltip" title="Go To List of groups under this service" data-placement="left">
				<span class="kt-subheader__btn-daterange-date" >GROUPS </span>
				<i class="flaticon2-cube-1"></i>
			</a>
		</div>
    </div>
</div>

<!-- end:: Content Head -->
@endsection