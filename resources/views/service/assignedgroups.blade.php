
@extends('layouts.vaslayout')

@section('content')
@include('service.header')
@yield('headercontent')
@include('notificationmessage.failed')
@yield('failed')
@include('notificationmessage.success')
@yield('success')
@section('headerbuttons')
@parent
	<li class="kt-menu__item  kt-menu__item--active " aria-haspopup="true"><a href="{{route('service.index')}}" class="kt-menu__link "><span class="kt-menu__link-text">Service</span></a></li>
									
				
@endsection
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
	<div class="kt-portlet kt-portlet--mobile">
		<div class="kt-portlet__head kt-portlet__head--lg">
			<div class="kt-portlet__head-label">
				<span class="kt-portlet__head-icon">
					<i class="kt-font-brand flaticon2-line-chart"></i>
				</span>
				<h3 class="kt-portlet__head-title">
					ASSIGNED GROUPS
					<small>LIST OF GROUPS ASSIGNED UNDER {{$service->service_name}}.</small>
				</h3>
			</div>
			<div class="kt-portlet__head-toolbar">
				<div class="kt-portlet__head-wrapper">
					<div class="kt-portlet__head-actions">
						<div class="dropdown dropdown-inline">
							
							<div class="dropdown-menu dropdown-menu-right">
								<ul class="kt-nav">
									<li class="kt-nav__section kt-nav__section--first">
										<span class="kt-nav__section-text">Choose an option</span>
									</li>
									<li class="kt-nav__item">
										<a href="#" class="kt-nav__link">
											<i class="kt-nav__link-icon la la-print"></i>
											<span class="kt-nav__link-text">Print</span>
										</a>
									</li>
									<li class="kt-nav__item">
										<a href="#" class="kt-nav__link">
											<i class="kt-nav__link-icon la la-copy"></i>
											<span class="kt-nav__link-text">Copy</span>
										</a>
									</li>
									<li class="kt-nav__item">
										<a href="#" class="kt-nav__link">
											<i class="kt-nav__link-icon la la-file-excel-o"></i>
											<span class="kt-nav__link-text">Excel</span>
										</a>
									</li>
									<li class="kt-nav__item">
										<a href="#" class="kt-nav__link">
											<i class="kt-nav__link-icon la la-file-text-o"></i>
											<span class="kt-nav__link-text">CSV</span>
										</a>
									</li>
									<li class="kt-nav__item">
										<a href="#" class="kt-nav__link">
											<i class="kt-nav__link-icon la la-file-pdf-o"></i>
											<span class="kt-nav__link-text">PDF</span>
										</a>
									</li>
								</ul>
							</div>
						</div>
						&nbsp;
						
					</div>
				</div>
			</div>
		</div>
		<div class="kt-portlet__body">
			<!--begin: Search Form -->
		</div>
		<div class="kt-portlet__body kt-portlet__body--fit">
		<form method="POST" action="{{route('assignedgroup',$service->id)}}">
			@csrf
			<!--begin: Datatable -->
			<table class="kt-datatable" id="html_table" width="100%">
				<thead>
					<tr>
						<th title="Field #1">GROUP ID</th>
						<th title="Field #2">GROUP NAME</th>
						<th title="Field #3"></th>
					</tr>
				</thead>
				<tbody>
				
				@foreach($groups as $group)
					<tr>
						<td>{{$group->id}}</td>
                        <td>{{$group->group_name}}</td>
                        <td>
						</td>
                    </tr>
                @endforeach
					
				</tbody>
			</table>
			<!--end: Datatable -->
		</div>
					

		</form>
	</div>
</div>
@endsection