@extends('layouts.vaslayout')
@section('content')

<!-- begin:: Content Head -->
<div class="kt-subheader   kt-grid__item" id="kt_subheader">
							<div class="kt-subheader__main">
								
								<a href="{{route('service.index')}}" class="btn btn-label-warning btn-bold btn-sm btn-icon-h kt-margin-l-10">
									SERVICE INDEX
								</a>
								<div class="kt-input-icon kt-input-icon--right kt-subheader__search kt-hidden">
									<input type="text" class="form-control" placeholder="Search order..." id="generalSearch">
									<span class="kt-input-icon__icon kt-input-icon__icon--right">
										<span><i class="flaticon2-search-1"></i></span>
									</span>
								</div>
							</div>
						</div>

						<!-- end:: Content Head -->
@include('notificationmessage.failed')
@yield('failed')
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
	<div class="row">
		<div class="col-xl-12">
			<!--begin::Portlet-->
			<div class="kt-portlet kt-portlet--head--noborder kt-portlet--height-fluid">
				<div class="kt-portlet__head">
					<div class="kt-portlet__head-label">
						<h3 class="kt-portlet__head-title">
							EDIT SERVICE 
						</h3>
					</div>
				</div>
				<!--begin::Form-->
				<form class="kt-form kt-form--label-right" id="kt_form_1" method="POST" action="{{route('service.update',$service->id)}}">
                        <input type="hidden" name="_token" value="{{csrf_token()}}">
                        <input type="hidden" name="_method" value="PUT">
					<div class="kt-portlet__body">
						
						<div class="form-group row">
						
							<div class="col-lg-6">
								<label class="">Service Name:</label>
								<div class="input-group">
								<input type="text" name="service_name" value="{{$service->service_name}}"class="form-control" placeholder="Enter service name">
									<div class="input-group-append"><a href="#" class="btn btn-icon"></a></div>
								</div>
								<span class="form-number text-danger">{{$errors->first('service_name')}}</span>
							</div>
						</div>
						<div class="form-group row">
							<div class="col-lg-6">
								<label class="">Description:</label>
								<input type="textarea" name="description" value="{{$service->description}}"class="form-control" placeholder="Enter description">
								<span class="form-text text-danger">{{$errors->first('description')}}</span>
							</div>
							
						</div>
						
					</div>
					<div class="kt-portlet__foot">
						<div class="kt-form__actions">
							<div class="row">
								<div class="col-lg-6">
									<button type="submit" class="btn btn-primary">Update</button>
								</div>
								<div class="col-lg-6 kt-align-right">
									<button type="reset" class="btn btn-danger">Delete</button>
								</div>
							</div>
						</div>
					</div>
				</form>
				<!--end::Form-->
			</div>
            <!--end::Portle -->
        </div>
    </div>
</div>
@endsection