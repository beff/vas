
@extends('layouts.vaslayout')
@section('content')
<<<<<<< HEAD
=======
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
<!--Begin::Dashboard 1-->
>>>>>>> d68a435fd33baac5003520e8e90b3e878bcc328d
<!--Begin::Section for total count-->
	<div class="row">
	    <div class="col-xl-4">
            <div class="kt-portlet kt-portlet--fit kt-portlet--head-lg kt-portlet--head-overlay kt-portlet--skin-solid kt-portlet--height-fluid">
                <div class="kt-portlet__body kt-portlet__body--fit">
					<div class="kt-widget17">
						<div class="kt-widget17__visual kt-widget17__visual--chart kt-portlet-fit--top kt-portlet-fit--sides" style="background-color: #fd397a">
							<div class="kt-widget17__chart" style="height:100px;">
								<canvas id="kt_chart_activities"></canvas>
							</div>
						</div>
						<div class="kt-widget17__stats">
							<div class="kt-widget17__items">
								<div class="kt-widget17__item">
									<span class="kt-widget17__icon">
										<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon kt-svg-icon--brand">
											<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
												<rect id="bound" x="0" y="0" width="24" height="24" />
												<path d="M5,3 L6,3 C6.55228475,3 7,3.44771525 7,4 L7,20 C7,20.5522847 6.55228475,21 6,21 L5,21 C4.44771525,21 4,20.5522847 4,20 L4,4 C4,3.44771525 4.44771525,3 5,3 Z M10,3 L11,3 C11.5522847,3 12,3.44771525 12,4 L12,20 C12,20.5522847 11.5522847,21 11,21 L10,21 C9.44771525,21 9,20.5522847 9,20 L9,4 C9,3.44771525 9.44771525,3 10,3 Z" id="Combined-Shape" fill="#000000" />
												<rect id="Rectangle-Copy-2" fill="#000000" opacity="0.3" transform="translate(17.825568, 11.945519) rotate(-19.000000) translate(-17.825568, -11.945519) " x="16.3255682" y="2.94551858" width="3" height="18" rx="1" />
											</g>
										</svg> </span>
									<span class="kt-widget17__subtitle">
										Total Customers
									</span>
									<span class="kt-widget17__desc">
									{{$totalcustomer}}
									</span>
								</div>
								<div class="kt-widget17__item">
									<span class="kt-widget17__icon">
										<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon kt-svg-icon--success">
											<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
												<polygon id="Bound" points="0 0 24 0 24 24 0 24" />
												<path d="M12.9336061,16.072447 L19.36,10.9564761 L19.5181585,10.8312381 C20.1676248,10.3169571 20.2772143,9.3735535 19.7629333,8.72408713 C19.6917232,8.63415859 19.6104327,8.55269514 19.5206557,8.48129411 L12.9336854,3.24257445 C12.3871201,2.80788259 11.6128799,2.80788259 11.0663146,3.24257445 L4.47482784,8.48488609 C3.82645598,9.00054628 3.71887192,9.94418071 4.23453211,10.5925526 C4.30500305,10.6811601 4.38527899,10.7615046 4.47382636,10.8320511 L4.63,10.9564761 L11.0659024,16.0730648 C11.6126744,16.5077525 12.3871218,16.5074963 12.9336061,16.072447 Z" id="Shape" fill="#000000" fill-rule="nonzero" />
												<path d="M11.0563554,18.6706981 L5.33593024,14.122919 C4.94553994,13.8125559 4.37746707,13.8774308 4.06710397,14.2678211 C4.06471678,14.2708238 4.06234874,14.2738418 4.06,14.2768747 L4.06,14.2768747 C3.75257288,14.6738539 3.82516916,15.244888 4.22214834,15.5523151 C4.22358765,15.5534297 4.2250303,15.55454 4.22647627,15.555646 L11.0872776,20.8031356 C11.6250734,21.2144692 12.371757,21.2145375 12.909628,20.8033023 L19.7677785,15.559828 C20.1693192,15.2528257 20.2459576,14.6784381 19.9389553,14.2768974 C19.9376429,14.2751809 19.9363245,14.2734691 19.935,14.2717619 L19.935,14.2717619 C19.6266937,13.8743807 19.0546209,13.8021712 18.6572397,14.1104775 C18.654352,14.112718 18.6514778,14.1149757 18.6486172,14.1172508 L12.9235044,18.6705218 C12.377022,19.1051477 11.6029199,19.1052208 11.0563554,18.6706981 Z" id="Path" fill="#000000" opacity="0.3" />
											</g>
										</svg> </span>
									<span class="kt-widget17__subtitle">
										Total Service Types 
									</span>
									<span class="kt-widget17__desc">
									{{$totalservice}}
									</span>
								</div>
							</div>
							<div class="kt-widget17__items">
								<div class="kt-widget17__item">
									<span class="kt-widget17__icon">
										<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon kt-svg-icon--warning">
											<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
												<rect id="bound" x="0" y="0" width="24" height="24" />
												<path d="M12.7037037,14 L15.6666667,10 L13.4444444,10 L13.4444444,6 L9,12 L11.2222222,12 L11.2222222,14 L6,14 C5.44771525,14 5,13.5522847 5,13 L5,3 C5,2.44771525 5.44771525,2 6,2 L18,2 C18.5522847,2 19,2.44771525 19,3 L19,13 C19,13.5522847 18.5522847,14 18,14 L12.7037037,14 Z" id="Combined-Shape" fill="#000000" opacity="0.3" />
												<path d="M9.80428954,10.9142091 L9,12 L11.2222222,12 L11.2222222,16 L15.6666667,10 L15.4615385,10 L20.2072547,6.57253826 C20.4311176,6.4108595 20.7436609,6.46126971 20.9053396,6.68513259 C20.9668779,6.77033951 21,6.87277228 21,6.97787787 L21,17 C21,18.1045695 20.1045695,19 19,19 L5,19 C3.8954305,19 3,18.1045695 3,17 L3,6.97787787 C3,6.70173549 3.22385763,6.47787787 3.5,6.47787787 C3.60510559,6.47787787 3.70753836,6.51099993 3.79274528,6.57253826 L9.80428954,10.9142091 Z" id="Combined-Shape" fill="#000000" />
											</g>
										</svg> </span>
									<span class="kt-widget17__subtitle">
								        Total Groups
									</span>
									<span class="kt-widget17__desc">
										{{$totalgroup}}
									</span>
								</div>
								<div class="kt-widget17__item">
									<span class="kt-widget17__icon">
										<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon kt-svg-icon--danger">
											<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
												<rect id="bound" x="0" y="0" width="24" height="24" />
												<path d="M3,16 L5,16 C5.55228475,16 6,15.5522847 6,15 C6,14.4477153 5.55228475,14 5,14 L3,14 L3,12 L5,12 C5.55228475,12 6,11.5522847 6,11 C6,10.4477153 5.55228475,10 5,10 L3,10 L3,8 L5,8 C5.55228475,8 6,7.55228475 6,7 C6,6.44771525 5.55228475,6 5,6 L3,6 L3,4 C3,3.44771525 3.44771525,3 4,3 L10,3 C10.5522847,3 11,3.44771525 11,4 L11,19 C11,19.5522847 10.5522847,20 10,20 L4,20 C3.44771525,20 3,19.5522847 3,19 L3,16 Z" id="Combined-Shape" fill="#000000" opacity="0.3" />
												<path d="M16,3 L19,3 C20.1045695,3 21,3.8954305 21,5 L21,15.2485298 C21,15.7329761 20.8241635,16.200956 20.5051534,16.565539 L17.8762883,19.5699562 C17.6944473,19.7777745 17.378566,19.7988332 17.1707477,19.6169922 C17.1540423,19.602375 17.1383289,19.5866616 17.1237117,19.5699562 L14.4948466,16.565539 C14.1758365,16.200956 14,15.7329761 14,15.2485298 L14,5 C14,3.8954305 14.8954305,3 16,3 Z" id="Rectangle-102-Copy" fill="#000000" />
											</g>
										</svg> </span>
									<span class="kt-widget17__subtitle">
										Total Revenue 
									</span>
									<span class="kt-widget17__desc">
										{{$totalrevenue}} birr.
									</span>
								</div>
							</div>
							<div class="kt-widget17__items">
								<div class="kt-widget17__item">
									<span class="kt-widget17__icon">
									<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
									    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
									        <rect id="bound" x="0" y="0" width="24" height="24"/>
									        <path d="M5,9 L19,9 C20.1045695,9 21,9.8954305 21,11 L21,20 C21,21.1045695 20.1045695,22 19,22 L5,22 C3.8954305,22 3,21.1045695 3,20 L3,11 C3,9.8954305 3.8954305,9 5,9 Z M18.1444251,10.8396467 L12,14.1481833 L5.85557487,10.8396467 C5.4908718,10.6432681 5.03602525,10.7797221 4.83964668,11.1444251 C4.6432681,11.5091282 4.77972206,11.9639747 5.14442513,12.1603533 L11.6444251,15.6603533 C11.8664074,15.7798822 12.1335926,15.7798822 12.3555749,15.6603533 L18.8555749,12.1603533 C19.2202779,11.9639747 19.3567319,11.5091282 19.1603533,11.1444251 C18.9639747,10.7797221 18.5091282,10.6432681 18.1444251,10.8396467 Z" id="Combined-Shape" fill="#000000"/>
									        <path d="M11.1288761,0.733697713 L11.1288761,2.69017121 L9.12120481,2.69017121 C8.84506244,2.69017121 8.62120481,2.91402884 8.62120481,3.19017121 L8.62120481,4.21346991 C8.62120481,4.48961229 8.84506244,4.71346991 9.12120481,4.71346991 L11.1288761,4.71346991 L11.1288761,6.66994341 C11.1288761,6.94608579 11.3527337,7.16994341 11.6288761,7.16994341 C11.7471877,7.16994341 11.8616664,7.12798964 11.951961,7.05154023 L15.4576222,4.08341738 C15.6683723,3.90498251 15.6945689,3.58948575 15.5161341,3.37873564 C15.4982803,3.35764848 15.4787093,3.33807751 15.4576222,3.32022374 L11.951961,0.352100892 C11.7412109,0.173666017 11.4257142,0.199862688 11.2472793,0.410612793 C11.1708299,0.500907473 11.1288761,0.615386087 11.1288761,0.733697713 Z" id="Shape" fill="#000000" fill-rule="nonzero" opacity="0.3" transform="translate(11.959697, 3.661508) rotate(-90.000000) translate(-11.959697, -3.661508) "/>
									    </g>
									</svg> </span>
									<span class="kt-widget17__subtitle">
								        Total Outgoing Messages
									</span>
									<span class="kt-widget17__desc">
									{{$totaloutgoingmessage}} sent messages.
									</span>
								</div>
								<div class="kt-widget17__item">
									<span class="kt-widget17__icon">
									<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
									    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
									        <rect id="bound" x="0" y="0" width="24" height="24"/>
									        <path d="M5,9 L19,9 C20.1045695,9 21,9.8954305 21,11 L21,20 C21,21.1045695 20.1045695,22 19,22 L5,22 C3.8954305,22 3,21.1045695 3,20 L3,11 C3,9.8954305 3.8954305,9 5,9 Z M18.1444251,10.8396467 L12,14.1481833 L5.85557487,10.8396467 C5.4908718,10.6432681 5.03602525,10.7797221 4.83964668,11.1444251 C4.6432681,11.5091282 4.77972206,11.9639747 5.14442513,12.1603533 L11.6444251,15.6603533 C11.8664074,15.7798822 12.1335926,15.7798822 12.3555749,15.6603533 L18.8555749,12.1603533 C19.2202779,11.9639747 19.3567319,11.5091282 19.1603533,11.1444251 C18.9639747,10.7797221 18.5091282,10.6432681 18.1444251,10.8396467 Z" id="Combined-Shape" fill="#000000"/>
									        <path d="M11.1288761,0.733697713 L11.1288761,2.69017121 L9.12120481,2.69017121 C8.84506244,2.69017121 8.62120481,2.91402884 8.62120481,3.19017121 L8.62120481,4.21346991 C8.62120481,4.48961229 8.84506244,4.71346991 9.12120481,4.71346991 L11.1288761,4.71346991 L11.1288761,6.66994341 C11.1288761,6.94608579 11.3527337,7.16994341 11.6288761,7.16994341 C11.7471877,7.16994341 11.8616664,7.12798964 11.951961,7.05154023 L15.4576222,4.08341738 C15.6683723,3.90498251 15.6945689,3.58948575 15.5161341,3.37873564 C15.4982803,3.35764848 15.4787093,3.33807751 15.4576222,3.32022374 L11.951961,0.352100892 C11.7412109,0.173666017 11.4257142,0.199862688 11.2472793,0.410612793 C11.1708299,0.500907473 11.1288761,0.615386087 11.1288761,0.733697713 Z" id="Shape" fill="#000000" fill-rule="nonzero" opacity="0.3" transform="translate(11.959697, 3.661508) rotate(-270.000000) translate(-11.959697, -3.661508) "/>
									    </g>
									</svg> 
									</span>
									<span class="kt-widget17__subtitle">
										Total Incoming Messages
									</span>
									<span class="kt-widget17__desc">
									{{$totalincomingmessage}} recived messages.
									</span>
								</div>
							</div>
						</div>
					</div>
				</div>
            </div>
        </div>
    </div>
<<<<<<< HEAD
	<!-- end section for total count -->
	<!-- SEARCH WITH DATE -->
	<div class="kt-portlet__body">
	<form  action="{{route('revenueperdate')}}" method="get" autocomplete="off">
			<div class="row kt-margin-b-20">
				<div class="col-lg-3 kt-margin-b-10-tablet-and-mobile">
					<label>TRANSACTION BY DATE:</label>
					<div class="input-daterange input-group" id="kt_datepicker">
						<input type="text" class="form-control kt-input" name="start" placeholder="From" data-col-index="5"/>
						<div class="input-group-append">
							<span class="input-group-text"><i class="la la-ellipsis-h"></i></span>
						</div>
						<input type="text" class="form-control kt-input" name="end" placeholder="To" data-col-index="5"/>
					</div>
				<button class="btn btn-primary btn-brand--icon" type="submit">
						<span>
							<i class="la la-search"></i>
							<span>Search</span>
						</span>
					</button>
					&nbsp;&nbsp;
					<button class="btn btn-secondary btn-secondary--icon" id="kt_reset">
						<span>
							<i class="la la-close"></i>
							<span>Reset</span>
						</span>
					</button>
			</div>
			</div>
		
		</form>
		<input type="text" class="form-control " name="display" placeholder="Fromsdfkhfkjhdkjfsdh"/>
	</div>
	<!-- END OF SEARCH BY DATE -->
	<div class="kt-portlet kt-portlet--tabs kt-portlet--height-fluid">
		<div class="kt-portlet__head">
		<div class="kt-portlet__head-label">
			<h3 class="kt-portlet__head-title">
			SERVICES 
			</h3>
		</div>
		</div>

		<div class="kt-portlet__body">
		<!--Begin::Tab Content-->
		<div class="tab-content">
			<!--begin::tab 1 content-->
			<div>
				<!--begin::Widget 11--> 
				<div class="kt-widget11">
					<div class="table-responsive">					 
						<table class="table">
							<thead>
								<tr>
									<td style="width:40%">Services</td>
									<td style="width:14%">Customers</td>
									<td style="width:15%">Groups</td>
									<td style="width:15%">Commission</td>
									<td style="width:15%">Total Revenue</td>
								</tr>
							</thead>
							<tbody>
								
								@foreach($service as $service)
								<tr>
									<td>
										<a href="#" class="kt-widget11__title">	{{$service->service_name}}</a>
										<span class="kt-widget11__sub">{{$service->created_at->format('d/m/Y')}}</span>
									</td>
									<td>
											@php
=======
    <!-- end section for total count -->
    <!-- beign section for detail per service type -->
    <div class="row">
	<div class="col-xl-8">
		<!--begin:: Widgets/Best Sellers-->
		<div class="kt-portlet kt-portlet--height-fluid">
			<div class="kt-portlet__head">
				<div class="kt-portlet__head-label">
					<h3 class="kt-portlet__head-title">
						SERVICES 
					</h3>
				</div>
				
			</div>
			<div class="kt-portlet__body">
				<div class="tab-content">
                    <div class="tab-pane active" id="kt_widget5_tab1_content" aria-expanded="true">
						<div class="kt-widget5">
                            @foreach($service as $service)
							<div class="kt-widget5__item">
								<div class="kt-widget5__content">
									<div class="kt-widget5__section">
										<a href="#" class="kt-widget5__title">
											{{$service->service_name}}
										</a>
										<div class="kt-widget5__info">
											<span>Created at:</span>
											<span class="kt-font-info">{{$service->created_at}}</span>
										</div>
									</div>
								</div>
								<div class="kt-widget5__content">
									<div class="kt-widget5__stats">
										<span class="kt-widget5__number">
                                            @php
>>>>>>> d68a435fd33baac5003520e8e90b3e878bcc328d
                                            $services = \App\Service::find($service->id);
                                            $groups=$services->groups;
                                            $i=0;
                                            foreach($groups as $group){
                                                $groups = \App\Group::find($group->id);
                                                $customers=$groups->customers->count();
                                                $i=$i+$customers;
                                            }
                                            echo $i;
<<<<<<< HEAD
											@endphp
									</td>
									<td>  
											@php
                                            $services = \App\Service::find($service->id);
                                            $groups=$services->groups->count();
                                            echo $groups;
											@endphp
									</td>
									<td>
									@php
										$keywords = \App\Keyword::where('service_id','=',$service->id)->get()->all();
       									$messagecount=0;
       									foreach($keywords as $keyword)
       									{
       									    $totalmessages= \App\ozekimessagein::where('msg','=',$keyword->keyword)->count();
       									    $messagecount=$messagecount+$totalmessages;
       									}
        								//total Revenue generated message
        								$commission=\App\Commission::get()->first();
        								if($commission->commission != null ){
        								$commissionpercent=$commission->commission/100;
        								}
        								if($commission->commission == null){
        								   $commissionpercent=1;
        								   }
        								$price=\App\Price::get()->first();
        								if($price->price == null){
        								 $totalrevenue="Price Value must be assigned in";
        								}else{
        								 $totalrevenue=$price->price*$messagecount;
        								 $filteredrevenue=$totalrevenue-($totalrevenue*$commissionpercent);
        								}
										echo $totalrevenue-$filteredrevenue;
									@endphp
									birr
									</td>
									<td>
										{{$totalrevenue}} birr
									</td>
									</tr>
								@endforeach
							</tbody>									     
						</table>
					</div>
				</div>
				<!--end::Widget 11--> 						             
			</div>
			<!--end::tab 1 content-->
		</div>
		<!--End::Tab Content-->
		</div>
	</div>
  
=======
                                            @endphp
                                        </span>
										<span class="kt-widget5__sales">customers</span>
									</div>
									<div class="kt-widget5__stats">
										<span class="kt-widget5__number">
                                            @php
                                            $services = \App\Service::find($service->id);
                                            $groups=$services->groups->count();
                                            echo $groups;
                                            @endphp
                                        </span>
										<span class="kt-widget5__votes">groups</span>
									</div>
								</div>
                            </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
    </div>
    </div>
												
    <!-- end section for detail per service type -->
</div>
>>>>>>> d68a435fd33baac5003520e8e90b3e878bcc328d
@endsection