@extends('layouts.vaslayout')

@section('content')
@include('group.header')
@yield('headercontent')
@include('notificationmessage.failed')
@yield('failed')
@include('notificationmessage.success')
@yield('success')
@section('headerbuttons')
@parent
	<li class="kt-menu__item  kt-menu__item--active " aria-haspopup="true"><a href="{{route('group.index')}}" class="kt-menu__link "><span class="kt-menu__link-text">Group</span></a></li>
									
				
@endsection
<!-- begin:: Content Head -->


<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
	<div class="row">
		<div class="col-xl-12">
			<!--begin::Portlet-->
			<div class="kt-portlet kt-portlet--head--noborder kt-portlet--height-fluid">
				<div class="kt-portlet__head">
					<div class="kt-portlet__head-label">
						<h3 class="kt-portlet__head-title">
							EDIT GROUP 
						</h3>
					</div>
				</div>
				<!--begin::Form-->
				<form class="kt-form kt-form--label-right" id="kt_form_1" method="POST" action="{{route('group.update',$group->id)}}">
                        <input type="hidden" name="_token" value="{{csrf_token()}}">
                        <input type="hidden" name="_method" value="PUT">
					<div class="kt-portlet__body">
						<div class="form-group row">
							<div class="col-lg-6">
								<label class="">Group Name:</label>
								<div class="input-group">
								<input type="text" name="group_name" value="{{$group->group_name}}"class="form-control" placeholder="Enter Group Name">
									<div class="input-group-append"><a href="#" class="btn btn-brand btn-icon"><i class="la la-users"></i></a></div>
								</div>
								<span class="form-number text-danger">{{$errors->first('group_name')}}</span>
							</div>
							
	
						</div>
						<div class="form-group row">
							<div class="col-lg-6">
								<label class="">Description:</label>
								<input type="textarea" name="description" value="{{$group->description}}"class="form-control" placeholder="Enter description">
								<span class="form-text text-danger">{{$errors->first('description')}}</span>
							</div>
							
						</div>
						
					</div>
					<div class="kt-portlet__foot">
						<div class="kt-form__actions">
							<div class="row">
								<div class="col-lg-6">
									<button type="submit" class="btn btn-primary">Update</button>
								</div>
								<div class="col-lg-6 kt-align-right">
									<button type="reset" class="btn btn-danger">Delete</button>
								</div>
							</div>
						</div>
					</div>
				</form>
				<!--end::Form-->
			</div>
            <!--end::Portle -->
        </div>
    </div>
</div>
@endsection