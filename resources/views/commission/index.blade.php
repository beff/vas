
@extends('layouts.vaslayout')
@section('content')
@include('commission.header')
@yield('headercontent')
@include('notificationmessage.failed')
@yield('failed')
@include('notificationmessage.success')
@yield('success')

<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
	<div class="kt-portlet kt-portlet--mobile">
		<div class="kt-portlet__head kt-portlet__head--lg">
			<div class="kt-portlet__head-label">
				<span class="kt-portlet__head-icon">
					<i class="kt-font-brand flaticon2-line-chart"></i>
				</span>
				<h3 class="kt-portlet__head-title">
					COMMISSION
					<small>VALUE OF COMMISSION</small>
				</h3>
			</div>
			<div class="kt-portlet__head-toolbar">
				<div class="kt-portlet__head-wrapper">
					<div class="kt-portlet__head-actions">
						<div class="dropdown dropdown-inline">
					
						</div>
						&nbsp;
						<a href="{{route('commission.create')}}" class="btn btn-brand btn-elevate btn-icon-sm">
							<i class="la la-plus"></i>
							Commission
						</a>
					</div>
				</div>
			</div>
		</div>
		<div class="kt-portlet__body">
			<!--begin: Search Form -->
		</div>
		<div class="kt-portlet__body kt-portlet__body--fit">
			<!--begin: Datatable -->
			<table class="kt-datatable" id="html_table" width="100%">
				<thead>
					<tr>
						<th title="Field #4">COMMISSION</th>
                        <th title="Field #7">ACTION</th>
					</tr>
				</thead>
				<tbody>
				@foreach($commission as $commission)
					<tr>
                        <td>{{$commission->commission}}</td>
                        <td>
						<form action="{{route('commission.destroy',$commission->id)}}" method="POST">
                		      @csrf
                		    <input type="hidden" name="_token" value="{{csrf_token()}}">
                		    <input type="hidden" name="_method" value="DELETE">
                		    <button type="submit" class="btn btn-outline-danger btn-icon mg-r-5"><div><i class="flaticon2-trash"></i></div></button>
                		</form> 
						</td>
                    </tr>
                @endforeach
					
				</tbody>
			</table>
			<!--end: Datatable -->
		</div>
	</div>
</div>
@endsection