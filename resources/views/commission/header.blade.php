@section('headercontent')
<!-- begin:: Content Head -->
<div class="kt-subheader   kt-grid__item" id="kt_subheader">
	<div class="kt-subheader__main">
		<a href="{{route('system.index')}}" class="btn btn-label-warning btn-bold btn-sm btn-icon-h kt-margin-l-10">
        <span> <i class="flaticon2-gear"></i></span>
            SYSTEM VARIABLE
		</a>
		<div class="kt-input-icon kt-input-icon--right kt-subheader__search kt-hidden">
			<input type="text" class="form-control" placeholder="Search order..." id="generalSearch">
			<span class="kt-input-icon__icon kt-input-icon__icon--right">
				<span><i class="flaticon2-search-1"></i></span>
			</span>
		</div>
	</div>
</div>

<!-- end:: Content Head -->
@endsection