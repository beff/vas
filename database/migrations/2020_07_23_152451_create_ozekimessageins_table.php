<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Eloquent\SoftDeletes;
class CreateOzekimessageinsTable extends Migration
{
    use SoftDeletes;
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ozekimessageins', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('sender', 30);
            $table->string('receiver', 30);
            $table->string('msg', 160);
            $table->timestamp('senttime');
            $table->string('recivedtime', 100);
            $table->string('operator', 30);
            $table->string('msgtype', 30);
            $table->string('reference', 30);
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ozekimessageins', function (Blueprint $table) {
            $table->dropSoftDeletes(); //add this line
        }); 
    }
}
