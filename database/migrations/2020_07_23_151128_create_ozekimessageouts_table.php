<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Eloquent\SoftDeletes;

class CreateOzekimessageoutsTable extends Migration
{
    use SoftDeletes;
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ozekimessageouts', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('sender', 30);
            $table->string('receiver', 30);
            $table->string('msg', 160);
            $table->timestamp('senttime');
            $table->string('recivedtime', 100)->nullable();
            $table->string('reference', 30)->nullable();
            $table->string('status', 30);
            $table->string('msgtype', 30);
            $table->string('operator', 100)->nullable();
            $table->string('errormsg', 250)->nullable();
            $table->integer('source_addr_ton')->nullable();
            $table->integer('source_addr_npi')->nullable();
            $table->integer('dest_addr_ton')->nullable();
            $table->integer('dest_addr_npi')->nullable();
            $table->integer('esm_class')->nullable();
            $table->integer('data_encoding')->nullable();
            $table->bigInteger('group_id')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ozekimessageouts', function (Blueprint $table) {
            $table->dropSoftDeletes(); //add this line
        }); 
    }
}
