<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('command', function () {
	/* php artisan migrate */
    \Artisan::call('migrate');
    dd("Done");
});
Route::get('/', function () {
    return view('auth.login');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home')->middleware('admincheck');
//DASHBOARD INDEX
Route::get('/dashboard', 'DashboardController@index')->name('dashboard.index')->middleware('admincheck');

Route::group(['middleware' => ['role:Super Admin']], function () {

//USERS 
Route::resource('users', 'UserController')->middleware('admincheck');
//ROLES
Route::resource('role', 'RoleController')->middleware('admincheck');
//PERMISSION 
Route::resource('permission', 'PermissionController')->middleware('admincheck');
//SERVICE
Route::get('/dashboard/{id}', 'ServiceController@servicedashboard')->name('servicedashboard')->middleware('admincheck');
Route::resource('service', 'ServiceController')->middleware('admincheck');
//SYSTEM VARIABLES
Route::resource('system', 'SystemController')->middleware('admincheck');
});

//CUSTOMER
Route::resource('customer', 'CustomerController')->middleware('admincheck');

//MESSAGES
Route::get('/message', 'MessageController@index')->name('message.index')->middleware('admincheck');
Route::get('/inbox', 'MessageController@inbox')->name('inbox')->middleware('admincheck');
//SENDING MESSAGES
Route::post('/send', 'MessageController@send')->name('send')->middleware('admincheck');
Route::get('/polling', 'MessageController@polling')->name('polling')->middleware('admincheck');
Route::get('/sending', 'MessageController@sending')->name('sending')->middleware('admincheck');
Route::get('/sent', 'MessageController@sent')->name('sent')->middleware('admincheck');
Route::get('/notsent', 'MessageController@notsent')->name('notsent')->middleware('admincheck');
Route::get('/delivered', 'MessageController@delivered')->name('delivered')->middleware('admincheck');
Route::get('/undelivered', 'MessageController@undelivered')->name('undelivered')->middleware('admincheck');
//SENDING MESSAGES PER SERVICE
Route::get('/message/{id}', 'MessageController@message')->name('message')->middleware('admincheck');
//GROUP
Route::resource('group', 'GroupController')->middleware('admincheck');
//ASSINGNING CUSTOMERS TO GROUPS
Route::get('/assign/{id}', 'GroupController@assignindex')->name('assignindex')->middleware('admincheck');
Route::post('/assigned/{id}', 'GroupController@assigned')->name('assigned')->middleware('admincheck');
Route::get('/assigned_customers/{id}', 'GroupController@assignedcustomers')->name('assigncustomers')->middleware('admincheck');
//ASSINGNING GROUPS TO SERVICE
Route::get('/assigngroup/{id}', 'ServiceController@assigngroupindex')->name('assigngroupindex')->middleware('admincheck');
Route::post('/assignedgroup/{id}', 'ServiceController@assignedgroup')->name('assignedgroup')->middleware('admincheck');
Route::get('/assigned_groups/{id}', 'ServiceController@assignedgroups')->name('assigngroups')->middleware('admincheck');
//KEYWORDS
Route::resource('keyword', 'KeywordController')->middleware('admincheck');
<<<<<<< HEAD
Route::post('/keywordservicestore', 'KeywordController@keywordservicestore')->name('keywordservicestore')->middleware('admincheck');
Route::get('/keywordservice/{id}', 'KeywordController@keywordservice')->name('keywordservice')->middleware('admincheck');
Route::get('/createkeyword/{id}', 'KeywordController@createkeyword')->name('createkeyword')->middleware('admincheck');
Route::DELETE('/keyworddestroy/{id}', 'KeywordController@keyworddestroy')->name('keyworddestroy')->middleware('admincheck');

//PRICE 
Route::resource('price', 'PriceController')->middleware('admincheck');
//COMMISSION 
Route::resource('commission', 'CommissionController')->middleware('admincheck');
//TOTAL REVENUE PER DATE RANGE 
Route::get('/revenueperdate', 'DashboardController@revenueperdate')->name('revenueperdate')->middleware('admincheck');
//TOTAL REVENUE PER DATE RANGE UNDER SERVICE TYPE
Route::get('/servicerevenueperdate/{id}', 'DashboardController@servicerevenueperdate')->name('servicerevenueperdate')->middleware('admincheck');
=======
Route::get('/keywordservicecreate/{id}', 'KeywordController@createkeyword')->name('keywordservicecreate')->middleware('admincheck');
Route::post('/keywordservicestore', 'KeywordController@keywordservicestore')->name('keywordservicestore')->middleware('admincheck');
Route::get('/keywordservice/{id}', 'KeywordController@keywordservice')->name('keywordservice')->middleware('admincheck');


//PRICE 
Route::resource('price', 'PriceController')->middleware('admincheck');
>>>>>>> d68a435fd33baac5003520e8e90b3e878bcc328d


