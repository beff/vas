<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class Group extends Model
{
    use SoftDeletes;
    
    /**
     * The groups has belonging to the customers.
     */
    public function customers()
    {
        return $this->belongsToMany('App\Customer','customer_group');
    }
     /**
     * The groups has belonging to the services.
     */
    public function services()
    {
        return $this->belongsToMany('App\Service','group_service');
    }
   
}
