<?php

namespace App\Http\Controllers;
use DB;
use Auth;
use Illuminate\Http\Request;
use Carbon\Carbon;
<<<<<<< HEAD
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
=======
>>>>>>> d68a435fd33baac5003520e8e90b3e878bcc328d
// use Illuminate\Database\Eloquent\Builder;
class ServiceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $services=\App\Service::all();
        return view('service.index',compact('services'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('service.create');
    }

    public function servicedashboard(Request $request,$id)
    {
        $Services = \App\Service::find($id);
        $groupcount=$Services->groups()->count();
<<<<<<< HEAD
        $keywords = \App\Keyword::where('service_id','=',$id)->get()->all();
        $messagecount=0;
        foreach($keywords as $keyword)
        {
            $totalmessages= \App\ozekimessagein::where('msg','=',$keyword->keyword)->count();
            $messagecount=$messagecount+$totalmessages;
        }
         //total Revenue generated message
         $commission=\App\Commission::get()->first();
         if($commission->commission != null ){
         $commissionpercent=$commission->commission/100;
         }
         if($commission->commission == null){
            $commissionpercent=1;
            }
         $price=\App\Price::get()->first();

         if($price->price == null){
          $totalrevenue="Price Value must be assigned in";
         }else{
          $totalrevenue=$price->price*$messagecount;
          $totalrevenue=$totalrevenue-($totalrevenue*$commissionpercent);
         }
        $service = DB::table('services')->where('id','=',$id)->get()->first();
        return view('service.dashboard',compact('id','service','groupcount','messagecount','totalrevenue'));
=======
        
        $service = DB::table('services')->where('id','=',$id)->get()->first();
        return view('service.dashboard',compact('id','service','groupcount'));
>>>>>>> d68a435fd33baac5003520e8e90b3e878bcc328d

    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       
        $request->validate([
            'service_name' => 'required|unique:services,service_name',

        ]);
        try{
            DB::beginTransaction();
        $services = new \App\Service;
        $services->service_name= $request->input('service_name');
        $services->description = $request->input('description');
        $Role = Role::create(['name' => $request->service_name]);
        $services->save();
        DB::commit();
        return redirect()->route('service.index')->with('success', 'New Service Has Been Added Successfully');
            } 
        catch (\Exception $e) {
        DB::rollback();
        // something went wrong
        return redirect()->back()->with('failed',$e->getMessage())->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $service = DB::table('services')->where('id',$id)->get()->first();
        return view('service.edit',compact('service'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $services = \App\Service::where('id','=',$id)->get()->first();
        if($services->service_name!=$request->input('service_name')){
        $request->validate([
        'service_name' => 'required|unique:services,service_name',
        
         ]);
        }
        
        try{
            DB::beginTransaction();
      
            $services->service_name = $request->input('service_name');
            $services->description = $request->input('description');
            $services->update();
            
        DB::commit();
        return redirect()->route('service.index')->with('success', 'Service Updated Successfully');
    } catch (\Exception $e) {
        DB::rollback();

        // something went wrong
        return redirect()->back()->with('failed',$e->getMessage())->withInput();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $service = \App\Service::where('id',$id)->delete();
        return redirect()->route('service.index')->with('success', 'Service Delete Successfully Wtih Their Group Membership');
    }
    public function assigngroupindex(Request $request,$id)
    {
        
        $Services = \App\Service::find($id);
        $group=$Services->groups()->where('service_id','=',$id)->get();
        $groups = DB::table('groups')
            ->join('group_service', 'groups.id', '=', 'group_service.group_id')
            ->where('group_service.service_id','!=',$id)
            ->get();
        $service = DB::table('services')->where('id','=',$id)->get()->first();
        return view('service.assign',compact('id','groups','service'));

    }
    public function assignedgroup(Request $request,$id)
    {
        $time = Carbon::now();
        try {
            DB::beginTransaction(); 
            //getting groups id 
            $groups=$request->groupid;
            $services=\App\Service::find($id);
           
            foreach($groups as $group){
              $services->groups()->attach($group);

            }
        DB::commit();
        return redirect()->route('assigngroupindex',$id)->with('success', 'Groups Assigned Successfully');
    } catch (\Exception $e) {
        DB::rollback();
       
        // something went wrong
        return redirect()->back()->with('failed', $e->getMessage())->withInput();
    }
     
    }
    public function assignedgroups(Request $request,$id)
    {
        //getting groups assigned for single service
        $services = \App\Service::find($id);
        $groups=$services->groups;
        $service = DB::table('services')->where('id','=',$id)->get()->first();
        return view('service.assignedgroups',compact('id','groups','service'));

    }
}
