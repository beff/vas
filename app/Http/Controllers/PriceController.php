<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class PriceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $price=\App\Price::get()->first();
        return view('price.index',compact('price'));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('price.create');
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'price' => 'required|unique:prices,price',
        ]);
     
        try {
            DB::beginTransaction(); 
            $prices = new \App\Price;
            $prices->price= $request->price;
            $prices->save();
        DB::commit();
        return redirect()->route('price.index')->with('success', 'Price Created Successfully');
    } catch (\Exception $e) {
        DB::rollback();
        // something went wrong
        return redirect()->back()->with('failed', $e->getMessage())->withInput();
    }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
<<<<<<< HEAD
        try{
            DB::beginTransaction();
        \App\Price::findOrFail($id)->delete();
        
        DB::commit();
        return redirect()->route('price.index')->with('success', 'Price has Deleted Successfully!!!');
        
    } catch (\Exception $e) {
        DB::rollback();

        // something went wrong
        return redirect()->back()->with('failed',$e->getMessage())->withInput();
        }
=======
        //
>>>>>>> d68a435fd33baac5003520e8e90b3e878bcc328d
    }
}
