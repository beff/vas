<?php

namespace App\Http\Controllers;
use DB;
use Illuminate\Http\Request;
use Auth;
class CustomerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       
        $customers=\App\Customer::all();
        return view('customer.index',compact('customers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('customer.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $request->validate([
            'phone' => 'required|unique:customers,phone',
            'address' => 'required',

        ]);
        $customers = new \App\Customer;
        $customers->phone= $request->input('phone');
        $customers->address = $request->input('address');
        $customers->description = $request->input('description');
        $customers->save();
        return redirect()->back()->with('message', 'Successfully Registered!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $customer = DB::table('customers')->where('id',$id)->get()->first();
        return view('customer.edit',compact('customer'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $customers = \App\Customer::where('id','=',$id)->get()->first();
        if($customers->phone!=$request->input('phone')){
        $request->validate([
        'phone' => 'required|unique:customers,phone',
        
         ]);
        }
        
        try{
            DB::beginTransaction();
      
            $customers->phone= $request->input('phone');
            $customers->address = $request->input('address');
            $customers->description = $request->input('description');
            $customers->update();
            
        DB::commit();
        return redirect()->route('customer.index')->with('success', 'Customer Profile Updated Successfully');
    } catch (\Exception $e) {
        DB::rollback();

        // something went wrong
        return redirect()->back()->with('failed',$e->getMessage())->withInput();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $customer = \App\Customer::where('id',$id)->delete();
        $customertogroup = \App\CustomerToGroup::where('customer_id',$id)->delete();
        return redirect()->route('customer.index')->with('success', 'Customer Delete Successfully Wtih Their Group Membership');
    }
}
