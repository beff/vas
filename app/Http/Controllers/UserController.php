<?php

namespace App\Http\Controllers;
use Auth;
use DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Spatie\Permission\Traits\HasRoles;

class UserController extends Controller
{
    use HasRoles;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = \App\User::
        join('user_variables', 'user_variables.user_id', '=', 'users.id')
        ->with('roles')
        ->get()->all();
        // $users = \App\User::join('model_has_roles', 'users.id', '=', 'model_has_roles.model_id')
        // ->join('roles', 'model_has_roles.role_id', '=', 'roles.id')
        // ->join('user_variables', 'user_variables.user_id', '=', 'users.id')
        // ->where('model_type', '=', 'App\\User')->get();
    //    dd($users);
        return view('users.index',compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roles=Role::get();
        $services = DB::table('services')->get()->all();
        return view('users.create',compact('roles','services'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => 'required|unique:users,email',
            'phone' => 'required|unique:users,phone',
            'password' => 'required',

        ]);
        try {
        DB::beginTransaction();

        $users = new \App\User;
        $users->first_name= $request->input('first_name');
        $users->last_name = $request->input('last_name');
        $users->middle_name = $request->input('middle_name');
        $users->phone = $request->input('phone');
        $users->email = $request->input('email');
        $users->password = Hash::make($request['password']);
        $users->save();

        $user = $users->id;
        $userrole = \App\User::findOrFail($user);
        $role = $request->role;
        $userrole->assignRole($role);

        $uservariable = new \App\UserVariable;
        $uservariable->service_id = $request->service;
        $uservariable->user_id = $user;
        $uservariable->save();
        
        DB::commit();
        return redirect()->back()->with('success', 'Successfully Registered!');
    } catch (\Exception $e) {
        DB::rollback();

        // something went wrong
        return redirect()->back()->with('failed', $e->getMessage())->withInput();
    }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // $user = \App\User::where('id',$id)->with('roles')->get()->first();
        $user = \App\User::where('users.id',$id)->join('model_has_roles', 'users.id', '=', 'model_has_roles.model_id')
        ->join('roles', 'model_has_roles.role_id', '=', 'roles.id')
        ->join('user_variables', 'user_variables.user_id', '=', 'users.id')
        ->where('model_type', '=', 'App\\User')->get()->first();
        $roles=Role::get();
        $services = DB::table('services')->get()->all();
        return view('users.edit',compact('id','user','roles','services'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
       

            $users = \App\User::where('id','=',$id)->get()->first();
            $uservariable = \App\UserVariable::where('user_id','=',$id)->get()->first();
            $request->validate([
            'first_name' => 'required',
            'last_name' => 'required',
            
             ]);
            if ($request->phone!=$users->phone && $request->email!=$users->email) {
            $request->validate([
                
                'email' => 'required|unique:users,email',
                'phone' => 'required|unique:users,phone',
            ]);
            }
            if ($request->phone!=$users->phone) {
                $request->validate([
                    'phone' => 'required|unique:users,phone',
                ]);
                }
                if ($request->email!=$users->email) {
                    $request->validate([
                        'email' => 'required|unique:users,email',
                        
                    ]);
                    }
            try{
                DB::beginTransaction();
          
                $users->first_name = $request->input('first_name');
                $users->last_name = $request->input('last_name');
                $users->middle_name = $request->input('middle_name');
                $users->update($request->all());
                $user = $id;
                $userrole = \App\User::findOrFail($user);
                $role = $request->role;
                $userrole->assignRole($role);
                $uservariable->service_id=$request->input('service');
                $uservariable->update();
            DB::commit();
            return redirect()->route('users.index')->with('success', 'Users Profile Updated Successfully');
        } catch (\Exception $e) {
            DB::rollback();

            // something went wrong
            return redirect()->back()->with('failed',$e->getMessage())->withInput();
            }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        
        $group = \App\User::where('id',$id)->delete();
        $customertogroup = \App\UserVariable::where('user_id',$id)->delete();

        return redirect()->route('users.index')->with('success', 'User Delete Successfully');
    }
}
