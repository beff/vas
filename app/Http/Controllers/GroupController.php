<?php

namespace App\Http\Controllers;
use DB;
use Carbon\Carbon;
use App\Customer;
use Illuminate\Http\Request;

class GroupController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $groups=\App\Group::all();

        return view('group.index',compact('groups'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('group.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
            $request->validate([
                'group_name' => 'required|unique:groups,group_name',
            ]);
         
            try {
                DB::beginTransaction(); 
                $groups = new \App\Group;
                $groups->group_name= $request->input('group_name');
                $groups->description = $request->input('description');
                $groups->save();
            DB::commit();
            return redirect()->route('group.index')->with('success', 'Group Created Successfully');
        } catch (\Exception $e) {
            DB::rollback();
           
            // something went wrong
            return redirect()->back()->with('failed', $e->getMessage())->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $customers = DB::table('customers')->get();
        $group = DB::table('groups')->where('id','=',$id)->get()->first();
        return view('group.edit',compact('group','id','customers'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $groups = \App\Group::where('id','=',$id)->get()->first();
        if($groups->group_name!=$request->input('group_name')){
        $request->validate([
        'group_name' => 'required|unique:groups,group_name',
        
         ]);
        }
        
        try{
            DB::beginTransaction();
      
            $groups->group_name = $request->input('group_name');
            $groups->description = $request->input('description');
            $groups->update();
            
        DB::commit();
        return redirect()->route('group.index')->with('success', 'Group Updated Successfully');
    } catch (\Exception $e) {
        DB::rollback();

        // something went wrong
        return redirect()->back()->with('failed',$e->getMessage())->withInput();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $group = \App\Group::where('id',$id)->delete();
        $customertogroup = \App\CustomerToGroup::where('group_id',$id)->delete();
        return redirect()->route('group.index')->with('success', 'Group Delete Successfully Wtih Their Customer Members');
    }
    public function assignindex(Request $request,$id)
    {
        //checking whether datas exist in pivot table or not

        $join = DB::table('customer_group')->get()->first();
        //retriving datas from customer table

        $customers = DB::table('customers')
        ->join('customer_group', 'customers.id', '=', 'customer_group.customer_id')
        ->where('customer_group.group_id','!=',$id)
        ->get();
        
        if($join = "null"){
        $customers = DB::table('customers')->get();
        }
        
        $group = DB::table('groups')->where('id','=',$id)->get()->first();
        return view('group.assign',compact('id','customers','group'));
   
    }
    public function assigned(Request $request,$id)
    {
        $time = Carbon::now();
        try {
            DB::beginTransaction(); 
            //getting customers id 
            $customers=$request->customerid;
            $groups=\App\Group::find($id);
           
            foreach($customers as $customer){
              $groups->customers()->attach($customer);

            }
        DB::commit();
        return redirect()->route('assignindex',$id)->with('success', 'Customers Assigned Successfully');
    } catch (\Exception $e) {
        DB::rollback();
       
        // something went wrong
        return redirect()->back()->with('failed', $e->getMessage())->withInput();
    }
     
    }
    public function assignedcustomers(Request $request,$id)
    {
        $groups = \App\Group::find($id);
        $customers=$groups->customers;
        $group = DB::table('groups')->where('id','=',$id)->get()->first();
        return view('group.assignedcustomers',compact('id','customers','group'));

    }
}
