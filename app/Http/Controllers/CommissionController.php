<?php

namespace App\Http\Controllers;
use DB;
use Illuminate\Http\Request;

class CommissionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $commission=\App\Commission::all();
        return view('commission.index',compact('commission'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('commission.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'commission' => 'required',

        ]);
     
        try {
            DB::beginTransaction(); 
            $commission = new \App\Commission;
            $commission->commission= $request->commission;
            $commission->save();
        DB::commit();
        return redirect()->route('commission.index')->with('success', 'Commission Created Successfully');
    } catch (\Exception $e) {
        DB::rollback();
       
        // something went wrong
        return redirect()->back()->with('failed', $e->getMessage())->withInput();
    }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{
            DB::beginTransaction();
        \App\Commission::findOrFail($id)->delete();
        
        DB::commit();
        return redirect()->route('commission.index')->with('success', 'Commission Value has Deleted Successfully!!!');
        
    } catch (\Exception $e) {
        DB::rollback();

        // something went wrong
        return redirect()->back()->with('failed',$e->getMessage())->withInput();
        }
    }
}
