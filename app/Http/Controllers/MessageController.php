<?php

namespace App\Http\Controllers;
use DB;
use Illuminate\Http\Request;
use Illuminate\Routing\UrlGenerator;
class MessageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
  
    public function index()
    {
        
        $groups=\App\Group::all();
        $services=\App\Service::all();
        $inbox = \App\ozekimessagein::get();
        return view('message.index',compact('groups','services','inbox'));
    }

    public function inbox()
    {
        
        $groups=\App\Group::all();
        $services=\App\Service::all();
        $inbox = \App\ozekimessagein::get();
        return view('message.index',compact('groups','services','inbox'));
    }
    public function send(Request $request)
    {
            $request->validate([
                'message' => 'required',
            ]);
             if($request->category == "group"){
                $request->validate([
                    'group' => 'required',
                ]);
             }
             if($request->category== "service"){
                $request->validate([
                    'service' => 'required',
                ]);
             }
            //  inputs from the form submitted
            $msg=$request->message;
            $phone=$request->phone;
            $group=$request->group;
            $service=$request->service;
            
        try {
            DB::beginTransaction();

            //sending to group
            if($request->category == "group"){

                $id=$request->group;
                $groups = \App\Group::find($id);
                $customers=$groups->customers;
                foreach ($customers as $customer){
                    
                    $this->ozekingsend($customer->phone,$msg,1000);
                    $this->sendingout($customer->phone,$msg,$id,1000);

                }
             }
             //sending to service
             if($request->category== "service"){
                $id=$request->service;
                // retriving groups under service 
                $services = \App\Service::find($id);
                $groups=$services->groups;
                foreach ($groups as $group){
                // retirving customers under eachgroup
                $id=$group->id;
                $groups = \App\Group::find($id);
                $customers=$groups->customers;

                foreach ($customers as $customer){
                    $this->ozekingsend($customer->phone,$msg,1000);
                    $this->sendingout($customer->phone,$msg,$id,1000);
                     }

                    }
             }
             //sending to single phone
             if($request->category== "phone"){
                    $this->ozekingsend($request->phone,$msg,1000);
                    $this->sendingout($request->phone,$msg,1000);

             }

           
    DB::commit();
    return redirect()->route('message.index')->with('success', 'Your Message Has Been Sent Successfully');
        } catch (\Exception $e) {
            DB::rollback();
        
            // something went wrong
            return redirect()->back()->with('failed', $e->getMessage())->withInput();
        }
    }
    public function sendingout($phone, $msg,$id,$debug=false)
    {
        try {
        DB::beginTransaction(); 
        $sending = new \App\ozekimessageout;
<<<<<<< HEAD
        $sending->sender= "6881";
=======
        $sending->sender= "6998";
>>>>>>> d68a435fd33baac5003520e8e90b3e878bcc328d
        $sending->receiver = $phone;
        $sending->msg = $msg;
        $sending->senttime = "2020-06-21 14:23:44";
        $sending->msgtype = "SMS:TEXT";
<<<<<<< HEAD
        $sending->status = "send";
=======
        $sending->status = "sent";
>>>>>>> d68a435fd33baac5003520e8e90b3e878bcc328d
        $sending->group_id = $id;
        $sending->save();
        DB::commit();
     
    } catch (\Exception $e) {
        DB::rollback();
    
        // something went wrong
        return redirect()->back()->with('failed', $e->getMessage())->withInput();
    }
    }
    public function ozekingsend($phone, $msg, $debug=false)
    {
        try {
            DB::beginTransaction(); 
        $ozeki_user = "admin";
<<<<<<< HEAD
        $ozeki_password = "admin123";
=======
        $ozeki_password = "yokida123";
>>>>>>> d68a435fd33baac5003520e8e90b3e878bcc328d
        $ozeki_url = "http://127.0.0.1:9501/api?";
        $url = 'username='.$ozeki_user;
        $url.= '&password='.$ozeki_password;
        $url.= '&action=sendmessage';
        $url.= '&messagetype=SMS:TEXT';
        $url.= '&recipient='.urlencode($phone);
        $url.= '&messagedata='.urlencode($msg);
        $urltouse =  $ozeki_url.$url;
        // if ($debug) { echo "Request: <br>$urltouse<br><br>"; }
    
        //Open the URL to send the message
        $response = $this->httpRequest($urltouse);
        // if ($debug) {
        //      echo "Response: <br><pre>".
        //      str_replace(array("<",">"),array("&lt;","&gt;"),$response).
        //      "</pre><br>"; }
    
    
          
        
        // }
    // $phonenum = $_POST['recipient'];
    // $message = $_POST['message'];
    // $debug = true;
    DB::commit();
     
    } catch (\Exception $e) {
        DB::rollback();
    
        // something went wrong
        return redirect()->back()->with('failed', $e->getMessage())->withInput();
    }
    }
    public function httpRequest($url){
        try {
            DB::beginTransaction(); 
        $pattern = "/http...([0-9a-zA-Z-.]*).([0-9]*).(.*)/";
       
        preg_match($pattern,$url,$args);
        $in = "";
       
        $fp = fsockopen("$args[1]", $args[2], $errno, $errstr, 30);
        if (!$fp) {
           return("$errstr ($errno)");
        } else {
            $out = "GET /$args[3] HTTP/1.1\r\n";
            $out .= "Host: $args[1]:$args[2]\r\n";
            $out .= "User-agent: Ozeki PHP client\r\n";
            $out .= "Accept: */*\r\n";
            $out .= "Connection: Close\r\n\r\n";
    
            fwrite($fp, $out);
            while (!feof($fp)) {
               $in.=fgets($fp, 128);
            }
        }
        fclose($fp);
        DB::commit();
        return $in;
    } catch (\Exception $e) {
        DB::rollback();
        // something went wrong
        return redirect()->back()->with('failed', $e->getMessage())->withInput();
    }
    }
    /////getting messages form ozeking incoming and outgoing messeages routes
    public function polling()
    {
        try {
            DB::beginTransaction(); 
            $polling = \App\ozekimessageout::where('status','=',"send")->get();
            DB::commit();
            return view('message.polling',compact('polling'));
    } catch (\Exception $e) {
        DB::rollback();
        // something went wrong
        return redirect()->back()->with('failed', $e->getMessage())->withInput();
    }
    }
    public function sending()
    {
        try {
            DB::beginTransaction(); 
            $groups=\App\Group::all();
            $services=\App\Service::all();
            $sending = \App\ozekimessageout::where('status','=',"sending")->get();
            DB::commit();
            return view('message.outbox',compact('sending','groups','services'));
     
    } catch (\Exception $e) {
        DB::rollback();
        // something went wrong
        return redirect()->back()->with('failed', $e->getMessage())->withInput();
    }
    }
    public function sent()
    {
        try {
            DB::beginTransaction(); 
            $groups=\App\Group::all();
            $services=\App\Service::all();
            $sent = \App\ozekimessageout::where('status','=',"sent")->get();
            DB::commit();
            return view('message.sent',compact('sent','groups','services'));
     
    } catch (\Exception $e) {
        DB::rollback();
        // something went wrong
        return redirect()->back()->with('failed', $e->getMessage())->withInput();
    }
    }
    public function notsent()
    {
        try {
            DB::beginTransaction(); 
            $groups=\App\Group::all();
            $services=\App\Service::all();
            $notsent = \App\ozekimessageout::where('status','=',"notsent")->get();
            DB::commit();
            return view('message.notsent',compact('notsent','groups','services'));
    } catch (\Exception $e) {
        DB::rollback();
        // something went wrong
        return redirect()->back()->with('failed', $e->getMessage())->withInput();
    }
    }
    public function delivered()
    {
        try {
            DB::beginTransaction(); 
            $groups=\App\Group::all();
            $services=\App\Service::all();
            $delivered = \App\ozekimessageout::where('status','=',"delivered")->get();
            DB::commit();
            return view('message.delivered',compact('delivered','groups','services'));
        } catch (\Exception $e) {
     
    } catch (\Exception $e) {
        DB::rollback();
        // something went wrong
        return redirect()->back()->with('failed', $e->getMessage())->withInput();
    }
    }
    public function undelivered()
    {
        try {
            DB::beginTransaction(); 
            $groups=\App\Group::all();
            $services=\App\Service::all();
            $undelivered = \App\ozekimessageout::where('status','=',"undelivered")->get();
            DB::commit();
            return view('message.undelivered',compact('undelivered','groups','services'));
     
    } catch (\Exception $e) {
        DB::rollback();
        // something went wrong
        return redirect()->back()->with('failed', $e->getMessage())->withInput();
    }
    }
    //function getting message page for services
    public function message(Request $request,$id)
    {
        $services = \App\Service::find($id);
        $groups=$services->groups;
        $inbox = \App\ozekimessagein::get();
        return view('message.message',compact('id','groups','inbox'));
    }

}
