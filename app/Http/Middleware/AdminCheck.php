<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class AdminCheck
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
      
        try{
            $user_variable =\App\UserVariable::where('user_id',Auth::user()->id)->get()->first();

            if(  $user_variable->user_id==null){
                return redirect('dashboard.index');
            }
            if(  $user_variable->user_id==null){
                return redirect('dashboard.index');
            }
        }
            catch(\Exception $ex){
                return redirect()->back()->with('failed', 'Session Has Been Destroyed Login Again');
            }
          
                return $next($request);
           
    }
   
}
