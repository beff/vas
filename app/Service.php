<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class Service extends Model
{
    use SoftDeletes;
    
    protected $dates = ['deleted_at'];
    //
      /**
     * The groups has belonging to the services.
     */
    public function groups()
    {
        return $this->belongsToMany('App\Group','group_service','service_id','group_id');
    }
    public function document($id)
    {
        return $this->hasMany('App\Group')->where('group_service.service_id','not like',$id)->get();
    }
    // total count of service
   
}
